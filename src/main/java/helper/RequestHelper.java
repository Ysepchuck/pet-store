package helper;

import petResponse.RequestPetModel;

import java.util.Collection;
import java.util.Collections;

public class RequestHelper {
    
    private static RequestPetModel pet = new RequestPetModel();
    public static RequestPetModel createPetDto (String name, long id, String status){
        
        pet.setName(name);
        pet.setId(id);
        pet.setStatus(status);
        return pet;
    }
    public static RequestPetModel updatePetDto (String newName, long newID, String newStatus){
        pet.setName(newName);
        pet.setId(newID);
        pet.setStatus(newStatus);
        return pet;
    }
    public static RequestPetModel createPetDtoDataProvider (long newID, String name){
        pet.setName(name);
        pet.setId(newID);
        return pet;
    }
    
}
