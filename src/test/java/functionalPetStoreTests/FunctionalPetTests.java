package functionalPetStoreTests;

import helper.PetController;
import org.assertj.core.api.SoftAssertions;
import org.testng.annotations.Test;
import petResponse.RequestPetModel;
import testBase.TestBase;

import static data.PetEndpoints.PET_STATUS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class FunctionalPetTests extends TestBase {
    long getPetId;
    int id = 1;
    RequestPetModel newPet = new RequestPetModel();
    PetController petController = new PetController(newPet);
    
    @Test(priority = 1)
    public void shouldCreatedPetTest() {
        
        petController.addNewPet(petName, petId, PET_STATUS);
        RequestPetModel resultPet = petController.addNewPet(petName, petId, PET_STATUS);
        getPetId = resultPet.getId();
        assertThat(resultPet.getId(), equalTo(petId));
    }
    
    @Test(enabled = false)
    public void shouldDeletePetTest() {
        petController.deletePet(1);
    }
    
    
    @Test(enabled = false)
    public void petShouldBeDeleted() {
        SoftAssertions assertions = new SoftAssertions();
        petController.searchPetIsDeleted(id).prettyPrint();
        assertions.assertThat(petController.addNewPet(petName, 2, PET_STATUS).getId());
        assertions.assertAll();
    }
}
