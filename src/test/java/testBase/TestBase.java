package testBase;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import rest.PetApiClient;

import java.util.Properties;

public class TestBase {
    Properties properties =new Properties();
    
    protected PetApiClient petApiClient;
    
    
    public String petName = properties.getProperty("petName");
    public long petId = 12132;
    
    @BeforeClass
    public void setUp() {
        petApiClient = new PetApiClient();
        
    }
    
    @AfterClass
    public void tearDown() {
    
    }
    
    @DataProvider
    public Object[][] dataProvider(){
        return new Object[][]{
                {3213,"Cat"},
                {2121,"Dog"}};
    }
    
    @DataProvider
    public Object[][] invalidData(){
        return new Object[][]{
                {3213,"Cat"},
                {2121,"Dog"}};
    }
}
