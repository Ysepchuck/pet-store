package findPetAndStoreTests;

import org.testng.annotations.Test;
import responseTests.ResponseTests;
import testBase.TestBase;

import static data.PetEndpoints.*;
import static io.restassured.RestAssured.given;

public class GetByStoreTests extends TestBase {
    
    @Test(enabled = false)
    public void shouldFindByStore() {
        given()
                .when()
                .get(URL+PET)
                .then()
                .statusCode(200).extract().response().prettyPrint();
    }
    
    
  
    
    @Test(enabled = false)
    public void shouldGetStoreByOrderId() {
        given()
                .when()
                .get(URL + PET +"/222222")
                .then()
                .statusCode(200).extract().response().prettyPrint();
    }
}
