package findPetAndStoreTests;

import helper.PetController;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;
import pet.PetResponse;
import petResponse.RequestPetModel;
import testBase.TestBase;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static data.PetEndpoints.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class FindPetByStatusTests extends TestBase {
    long petId = 13121;
    long newID = 545646464;
    long expectID;
    String petName = "Capita";
    RequestPetModel petModel = new RequestPetModel();
    PetController petController = new PetController(petModel);
    
    
    @Test
    public void updatePetTest() {
        
        petController.addNewPet(petName, petId, PET_STATUS);
        RequestPetModel updatePet = petController.updatePet(petName, newID, PET_STATUS);
        expectID = updatePet.getId();
        assertThat(expectID, is(equalTo(newID)));
    }
    
    @Test
    public void findPetByStatusPendingTest() {
        
        petController.addNewPet(petName, petId, PET_STATUS);
        
        PetResponse[] petResponse = given()
                                            .contentType(ContentType.JSON)
                                            .when()
                                            .get(URL + PET + FIND_BY_STATUS + STATUS_DEAD)
                                            .then()
                                            .statusCode(200).extract().response().as(PetResponse[].class);
        System.out.println(petResponse);
        List<PetResponse> filteredResponse = Arrays.asList(petResponse);
        List<PetResponse> newList = filteredResponse.stream()
                                            .filter(name -> name.getName() == null || name.getName().equalsIgnoreCase(petName))
                                            .collect(Collectors.toList());
        System.out.println(newList.size());
    }
    
}
