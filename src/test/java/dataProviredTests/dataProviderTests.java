package dataProviredTests;

import helper.PetController;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import pet.PetResponse;
import petResponse.RequestPetModel;
import testBase.TestBase;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;


public class dataProviderTests extends TestBase {

    RequestPetModel pet = new RequestPetModel();
    PetController petController = new PetController(pet);
    
    
    @Test(dataProvider = "dataProvider")
    public void shouldCreatedPetWithDataProvider(long petId, String petName) {
        PetResponse pet =petController.addNewPetWithDataProvider(petId,petName);
        assertThat(pet.getName(),equalTo(petName));
    }
}

